import os

from django.core.asgi import get_asgi_application

asgi_application = get_asgi_application()


from channels.routing import ProtocolTypeRouter
from core.middlewares.websocket import JWTAuthMiddlewareStack
from channels.routing import URLRouter
from core.apps.home.websocket.routing import urlpatterns


from config.env import env

os.environ.setdefault("DJANGO_SETTINGS_MODULE", env("DJANGO_SETTINGS_MODULE"))


application = ProtocolTypeRouter(
    {
        "http": asgi_application,
        "websocket": JWTAuthMiddlewareStack(URLRouter(urlpatterns)),
    }
)
