from rest_framework import serializers
from .. import models


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = [
            "chat",
            "user",
            "message",
            "is_read",
            "created_at",
            "updated_at",
        ]


class MessageMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = (
            "message",
            "is_read",
            "created_at",
            "updated_at",
        )
