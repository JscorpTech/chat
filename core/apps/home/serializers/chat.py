from rest_framework import serializers
from .. import models
from core.http import serializers as core_serializers
from .. import serializers as home_serializers


class ChatSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    message = serializers.SerializerMethodField()

    def get_user(self, obj):
        user = obj.users.exclude(id=self.context["request"].user.id).first()
        return core_serializers.UserSerializer(user).data

    def get_message(self, obj):
        message = (
            models.Message.objects.filter(chat_id=obj.id)
            .order_by("-created_at")
            .first()
        )
        return home_serializers.MessageMiniSerializer(message).data

    class Meta:
        model = models.Chat
        fields = ("user", "message", "created_at")
