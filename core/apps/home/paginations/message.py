from rest_framework import pagination


class MessagePagination(pagination.PageNumberPagination):
    page_size = 10  # Number of messages per page
    page_size_query_param = 'page_size'
    max_page_size = 100