from pydantic import BaseModel
from ..enums import ActionEnum
from typing import Union, List, Dict, Any


class Request(BaseModel):
    action: ActionEnum
    data: dict


class ResponseSuccess(BaseModel):
    success: bool = True
    action: Union[ActionEnum, None] = None
    code: int = 200
    detail: str = "OK"
    data: Union[List[Any], Dict, Any] = []
    errors: list = []


class ResponseError(BaseModel):
    success: bool = False
    code: int = 400
    detail: str = "Invalid Error"
    errors: list = []
    data: list = []
