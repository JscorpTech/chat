from pydantic import BaseModel
from typing import Union


class User(BaseModel):
    user_id: int
    first_name: str
    last_name: Union[str, None] = None
