from pydantic import BaseModel
from .user import User
from typing import Union


class SendMessage(BaseModel):
    chat_id: int
    message: str


class ChatCreate(BaseModel):
    user_id: int
    message: str


class ChatRetrieve(BaseModel):
    chat_id: int
    user: Union[User, None] = None
    message: Union[str, None] = None


class ChatList(BaseModel):
    chat_id: int
    name: str
    message: Union[str, None] = None


class ChatDelete(BaseModel):
    chat_id: int
