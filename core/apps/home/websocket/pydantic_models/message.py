from pydantic import BaseModel
from .user import User


class Message(BaseModel):
    chat_id: int
    user: User
    message: str
    is_read: bool

    created_at: str
    updated_at: str


class IsRead(BaseModel):
    message_id: int
    is_read: bool
    chat_id: int
