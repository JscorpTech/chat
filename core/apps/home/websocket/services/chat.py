from typing import Type, Union
from channels.generic.websocket import AsyncWebsocketConsumer
from .base import BaseWebSocketService
from .. import pydantic_models
from core.apps.home import models
from ..enums import ActionEnum


class ChatConsumerService(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.service = BaseWebSocketService()

    async def new_chat(
        self, data: dict, user
    ) -> Union[Type[pydantic_models.ChatCreate], None]:
        """
        Asynchronously creates a new chat between users.

        This method validates the incoming data against the ChatCreate Pydantic model,
        creates a new chat record, adds the users to the chat, and notifies the other user
        via the channel layer.

        Parameters:
        - data (dict): The data required to create a new chat. Expected to match the
          ChatCreate Pydantic model structure.
        - user: The user object of the user initiating the chat. This user will be added
          to the chat along with the user specified in the `data`.

        Returns:
        - Union[Type[pydantic_models.ChatCreate], None]: On successful creation, returns
          None after sending a success response to the initiating user. If validation fails,
          returns None after sending an error response.

        Exceptions:
        - ValueError: If the data validation fails, it catches a ValueError, logs it,
          and sends an error message back to the client.
        """
        try:
            validated_data = self.service.validate(
                pydantic_models.ChatCreate, data, raise_exception=True
            )
        except ValueError as e:
            print(e)
            return await self.send(
                text_data=pydantic_models.ResponseError(
                    errors=e.args[0], detail="Invalid Data"
                ).model_dump_json()
            )

        chat = await models.Chat.objects.acreate()
        await chat.users.aadd(validated_data.user_id)
        await chat.users.aadd(user.id)
        await chat.asave()
        await self.channel_layer.group_send(
            "user_{}".format(validated_data.user_id),
            {
                "type": "chat_message",
                "data": pydantic_models.ResponseSuccess(
                    action=ActionEnum.new_chat,
                    data=pydantic_models.ChatRetrieve(
                        chat_id=chat.id,
                        name=(await chat.users.afirst()).first_name,
                        user=pydantic_models.User(
                            user_id=user.id, first_name=user.first_name
                        ),
                        message=validated_data.message,
                    ),
                ).model_dump(),
            },
        )
        await self.send(
            text_data=pydantic_models.ResponseSuccess(
                detail="Chat Created"
            ).model_dump_json()
        )

    async def send_message(
        self, data: dict, user
    ) -> pydantic_models.SendMessage:
        """
        Asynchronously validates and returns the data for sending a message.

        This method takes a dictionary of data, validates it against the SendMessage Pydantic model,
        and returns the validated data. This is a preliminary step before actually sending the message
        to ensure that the data conforms to the expected structure and types defined in the model.

        Parameters:
        - data (dict): The data to be validated and sent as a message. Expected to match the
        SendMessage Pydantic model structure.

        Returns:
        - pydantic_models.SendMessage: The validated data as a SendMessage Pydantic model instance.
        """
        try:
            validated_data = BaseWebSocketService().validate(
                pydantic_models.SendMessage, data, raise_exception=True
            )
        except ValueError as e:
            print(e)
            return await self.send(
                text_data=pydantic_models.ResponseError(
                    errors=e.args[0], detail="Invalid Data"
                ).model_dump_json()
            )
        try:
            chat = await models.Chat.objects.aget(pk=validated_data.chat_id)
        except models.Chat.DoesNotExist:
            return await self.send(
                text_data=pydantic_models.ResponseError(
                    detail="Chat does not exist"
                ).model_dump_json()
            )
        except Exception as e:
            print(e)
        message = await models.Message.objects.acreate(
            chat=chat,
            user_id=user.id,
            message=validated_data.message,
        )
        await self.channel_layer.group_send(
            "chat_{}".format(validated_data.chat_id),
            {
                "type": "chat_message",
                "data": pydantic_models.ResponseSuccess(
                    action=ActionEnum.send_message,
                    data=pydantic_models.Message(
                        chat_id=validated_data.chat_id,
                        message=validated_data.message,
                        user=pydantic_models.User(
                            user_id=user.id, first_name=user.first_name
                        ),
                        is_read=message.is_read,
                        created_at=message.created_at.strftime(
                            "%Y-%m-%d %H:%M:%S"
                        ),
                        updated_at=message.updated_at.strftime(
                            "%Y-%m-%d %H:%M:%S"
                        ),
                    ),
                ).model_dump(),
            },
        )
        return validated_data

    async def delete_chat(self, data: dict) -> pydantic_models.ChatDelete:
        validated_data = pydantic_models.ChatDelete(**data)
        return validated_data
