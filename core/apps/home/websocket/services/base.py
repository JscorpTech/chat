from pydantic import ValidationError


class BaseWebSocketService:
    def validate(self, interface, data, raise_exception=False):
        """Validate data with interface.

        Args:
            interface (_type_): _description_
            data (_type_): _description_

        Returns:
            _type_: _description_
        """
        errors = []
        try:
            data = interface(**data)
        except ValidationError as e:
            for i in e.errors():
                errors.append(
                    {
                        "field": i["loc"][0],
                        "message": i["msg"],
                        "code": i["type"],
                    }
                )
        is_valid = len(errors) == 0
        if not is_valid and raise_exception:
            raise ValueError(errors)
        elif raise_exception:
            return data
        return data, errors, is_valid
