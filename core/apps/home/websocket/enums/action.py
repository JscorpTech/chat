from enum import Enum


class ActionEnum(str, Enum):
    new_chat = "new_chat"
    send_message = "send_message"
    delete_chat = "delete_chat"
    is_read = "is_read"
