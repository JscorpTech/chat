from asgiref.sync import sync_to_async
from core.apps.home import models


class Chat:
    @sync_to_async
    def get_chats(self):
        return list(models.Chat.objects.all().values())
