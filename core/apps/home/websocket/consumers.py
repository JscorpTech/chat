import json
from core.utils.console import Console
from . import pydantic_models
from . import services
from typing import Union, List
from .models import Chat as ChatModel


class ChatConsumer(services.ChatConsumerService):
    async def connect(self):
        if not self.scope["user"].is_authenticated:
            return await self.close()
        await self.groups_add(await self.get_groups())
        await self.accept()

    async def disconnect(self, close_code):
        self.groups_discard(await self.get_groups())

    async def receive(self, text_data):
        data = json.loads(text_data)
        user = self.scope["user"]
        data, errors, is_valid = services.BaseWebSocketService().validate(
            pydantic_models.Request, data
        )
        print(data)
        if not is_valid:
            return await self.send(text_data=json.dumps(errors))

        match data.action:
            case "new_chat":
                await self.new_chat(data.data, user)
            case "send_message":
                await self.send_message(data.data, user)
            case "delete_chat":
                await self.delete_chat(data.data)
            case _:
                Console().log(f"Unknown action: {data.action}")

    async def chat_message(self, event):
        await self.send(text_data=json.dumps(event))

    async def groups_add(self, groups: Union[List[str]]) -> None:
        for group in groups:
            await self.channel_layer.group_add(group, self.channel_name)

    async def groups_discard(self, groups: Union[List[str]]) -> None:
        for group in groups:
            await self.channel_layer.group_discard(group, self.channel_name)

    async def get_groups(self):
        return [
            "all",
            "user_{}".format(self.scope["user"].id),
            *[
                "chat_{}".format(chat.get("id"))
                for chat in await ChatModel().get_chats()
            ],
        ]
