from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from core.apps.home.websocket import pydantic_models as pym, enums


class ChatService:
    def __init__(self) -> None:
        self.layers = get_channel_layer()

    @async_to_sync
    async def websocket__is_read(self, message):
        return await self._send__emit(
            "chat_{}".format(message.chat.id),
            pym.ResponseSuccess(
                action=enums.ActionEnum.is_read,
                data=pym.IsRead(
                    chat_id=message.chat.id,
                    is_read=message.is_read,
                    message_id=message.id,
                ),
            ).model_dump(),
        )

    @async_to_sync
    async def websocket__delete_chat(self, chat):
        return await self._send__emit(
            "chat_{}".format(chat.id),
            pym.ResponseSuccess(
                action=enums.ActionEnum.delete_chat,
                data=pym.ChatDelete(
                    chat_id=chat.id,
                ),
            ).model_dump(),
        )

    async def _send__emit(self, group, data):
        return await self.layers.group_send(
            group,
            {
                "type": "chat_message",
                "data": data,
            },
        )
