from django.db import models
from core.http.models import User


class Message(models.Model):
    chat = models.ForeignKey(
        "Chat", related_name="messages", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User, related_name="messages", on_delete=models.CASCADE
    )
    message = models.CharField(max_length=255)
    is_read = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return "{} | {}".format(self.message, self.user.first_name)

    class Meta:
        ...
