from django.db import models
from core.http import models as http_models


class Chat(models.Model):
    users = models.ManyToManyField(http_models.User, related_name="chats")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.created_at.strftime("%Y-%m-%d %H:%M:%S")

    class Meta:
        ordering = ["-created_at"]
