from typing import Any
from rest_framework.viewsets import ViewSet, GenericViewSet
from rest_framework.decorators import action
from core.apps.home import models
from rest_framework import exceptions, status, mixins, pagination
from rest_framework.response import Response
from ..services import ChatService
from .. import serializers as home_serializers
from .. import paginations as home_paginations


class ChatMethodsViewSet(ViewSet):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.service = ChatService()

    @action(methods=["GET"], detail=True, url_path="is-read")
    def is_read(self, request, pk):
        try:
            message = models.Message.objects.get(id=pk)
        except models.Message.DoesNotExist:
            raise exceptions.NotFound("Message does not exist")

        message.is_read = True
        message.save()
        self.service.websocket__is_read(message)
        return Response(
            data={"detail": "Message is read"}, status=status.HTTP_200_OK
        )

    @action(methods=["DELETE"], detail=True, url_path="delete-chat")
    def is_unread(self, request, pk):
        try:
            chat = models.Chat.objects.get(id=pk)
        except models.Chat.DoesNotExist:
            raise exceptions.NotFound("Chat does not exist")
        self.service.websocket__delete_chat(chat)
        chat.delete()

        return Response(
            data={"detail": "Chat is deleted"}, status=status.HTTP_200_OK
        )


class ChatViewSet(mixins.ListModelMixin, GenericViewSet):
    pagination_class = pagination.PageNumberPagination

    def get_queryset(self):
        return models.Chat.objects.filter(users=self.request.user)

    def get_serializer_class(self):
        return home_serializers.ChatSerializer

    @action(methods=["GET"], detail=True, url_path="messages")
    def messages(self, request, pk):
        try:
            chat = models.Chat.objects.get(id=pk)
        except models.Chat.DoesNotExist:
            raise exceptions.NotFound("Chat does not exist")

        messages = chat.messages.all()
        paginator = home_paginations.MessagePagination()  # Use the custom pagination class if defined
        page = paginator.paginate_queryset(messages, request)
        if page is not None:
            serializer = home_serializers.MessageMiniSerializer(page, many=True)
            return paginator.get_paginated_response(serializer.data)

        serializer = home_serializers.MessageMiniSerializer(messages, many=True)
        return Response(serializer.data)
