from django.contrib import admin
from unfold.admin import ModelAdmin
from core.apps.home.models import Chat, Message


class ChatAdmin(ModelAdmin):
    ...


class MessageAdmin(ModelAdmin):
    list_display = [
        "message",
        "first_name",
        "is_read",
        "created_at",
        "updated_at",
    ]

    def first_name(self, obj):
        return obj.user.first_name


admin.site.register(Message, MessageAdmin)
admin.site.register(Chat, ChatAdmin)
