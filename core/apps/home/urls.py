"""
Home app urls
"""

from django.urls import path
from django.urls import include

from rest_framework import routers

from core.apps.home import views

router = routers.DefaultRouter()
router.register("posts", views.PostListView, basename="posts")
router.register(
    "chat-methos", views.ChatMethodsViewSet, basename="chat-methods"
)
router.register("chats", views.ChatViewSet, basename="chats")

urlpatterns = [
    path(
        "translations/",
        views.FrontendTranslationView.as_view(),
        name="frontend-translation",
    ),  # noqa
    path("", include(router.urls), name="posts"),
    path("", views.HomeView.as_view(), name="home"),
]
